FROM ruby:3.0.1-alpine3.13

RUN apk add g++ make
RUN gem install bundler:2.2.17

EXPOSE 3000

WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . /app

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "--port", "3000"]
