require "minitest/autorun"
require "rack/test"

require "app"

describe "App" do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it "welcomes visitors" do
    get "/"

    assert last_response.ok?
    assert_match "Welcome", last_response.body
  end
end
