.PHONY: build clean start shell test

build:
	docker-compose build

start:
	docker-compose up

shell:
	docker-compose run --rm app sh

test:
	docker-compose run --rm app rake test

clean:
	docker-compose down --volumes
